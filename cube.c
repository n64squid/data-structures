/* ==========================================================================
	  ____      _ 
	 / ____   _| |__   ___   ___
	| |  | | | | '_ \ / _ \ / __|
	| |__| |_| | |_) |  __/| (__
	 \____\__,_|_.__/ \___(_\___|

	This is a library for the use of a Binary Search Cube. Well, in reality,
	it is a Binary Search Tesseract since we are working with 4 dimensions.

	It's basically a 4D array that holds elements in order which allows us to
	insert and search them in O(log(n)) time.

	The ideal structure occurs where all the axes are of equal length. Random
	input gives a close approximation to a perfect cube, but ordered input makes
	it very lobsided and more inefficient. A cube can be rebalanced using the
	rebalance_cube() function to make it more square. Inputting n elements takes
	O(nlog(n)) time, but rebalancing only takes O(n) time.

	The splitting functionality ensures that no axis is shorter than half of the
	max length, helping to contribute to keeping it balanced

	An ordered cube can then be put into an array using the cube_to_array()
	function. This takes O(n) time.

	TODO:
		-	Add functionality for removing elements by value or by index
		-	Merge two adjacent axis if they are short enough
		-	This makes a 4D cube, but a lot of the code is similar so
			it could be remade into an N-dimensional cube with more generic
			functions

	Note:	Other people who have built a BSC generally made each axis an array
			of pointers to nodes, but I just made it an array of nodes. This
			increases the size of the memory to be copied in a split, but it
			reduces overall memory needed to store the structure and reduces
			its complexity a little bit.

 ========================================================================== */

#include "cube.h"
#include "basic.h"
#include "get_set_funcs.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void w_split(cube* c, unsigned int w);
void x_split(cube* c, unsigned int w, unsigned int x);
void y_split(cube* c, unsigned int w, unsigned int x, unsigned int y);

// Initialise the cube structure
// Define two function pointers for value retrieval and to set them back
// If you're not going to do any setting, you can leave the 2nd param as NULL
void cube_init(cube* c, int(*get_value)(void*), void(*set_value)(void*, void*)) {
	c->w_axis = NULL;
	c->w_size = 0;
	c->volume = 0;
	c->get_value = get_value;
	c->set_value = set_value;
	c->axis_length = BSC_LENGTH;
}

// Insert an item into the cube.
// The value must be a pointer to an element
// The element isn't stored in the cube so if it is modified, it might break the cube structure
void cube_insert(cube* c, void* val) {
	int w, x, y, z;	// Indices of the variable to insert
	int mid;		// Temp variable used for binary searches

	// Check to see if the cube is empty
	if (!c->w_size) {
		// This is a new cube, so this will be inserted at position #0
		w = x = y = z = 0;

		// Start by allocating the memory
		c->w_axis = malloc(sizeof(cube_w));
		c->w_axis[w].x_axis = malloc(sizeof(cube_x));
		c->w_axis[w].x_axis[x].y_axis = malloc(sizeof(cube_y));
		c->w_axis[w].x_axis[x].y_axis[y].z_values = malloc(sizeof(void*));

		// Set the size of all axes to 1
		c->w_size = c->w_axis[w].x_size = c->w_axis[w].x_axis[x].y_size = 1;
		// Except this one, which we will be inceasing in the insertion stage
		c->w_axis[w].x_axis[x].y_axis[y].z_size = 0;

		// Set the floor for all axes to the int version of the value
		c->w_axis[w].w_floor = c->w_axis[w].x_axis[x].x_floor = c->w_axis[w].x_axis[x].y_axis[y].y_floor = c->get_value(val);

	} else if (is_less_than(c->get_value(val), c->w_axis[0].w_floor)) {
		// This item is going to be pushed to the very left
		w = x = y = z = 0;
		// Set the floor for all axes to the int version of the value
		c->w_axis[w].w_floor = c->w_axis[w].x_axis[x].x_floor = c->w_axis[w].x_axis[x].y_axis[y].y_floor = c->get_value(val);
	} else {
		// We're going to need to search for the correct position for this item
		// Let's start by performing a binary search on each axis to find the correct W, X Y and Z coordinates
		
		// W-axis search
		mid = w = c->w_size - 1;
		while (mid > 7) {
			mid /= 2;
			if (is_less_than(c->get_value(val), c->w_axis[w-mid].w_floor)) {
				w -= mid;
			}
		}
		while (w>0 && is_less_than(c->get_value(val), c->w_axis[w].w_floor)) {
			w--;
		}
		
		// X-axis search
		mid = x = c->w_axis[w].x_size - 1;
		while (mid > 7) {
			mid /= 2;
			if (is_less_than(c->get_value(val), c->w_axis[w].x_axis[x-mid].x_floor)) {
				x -= mid;
			}
		}
		while (x>0 && is_less_than(c->get_value(val), c->w_axis[w].x_axis[x].x_floor)) {
			x--;
		}

		// Y-Axis search
		mid = y = c->w_axis[w].x_axis[x].y_size - 1;
		while (mid > 7) {
			mid /= 2;
			if (is_less_than(c->get_value(val), c->w_axis[w].x_axis[x].y_axis[y-mid].y_floor)) {
				y -= mid;
			}
		}
		while (y>0 && is_less_than(c->get_value(val), c->w_axis[w].x_axis[x].y_axis[y].y_floor)) {
			y--;
		}

		// Z-Axis search
		mid = z = c->w_axis[w].x_axis[x].y_axis[y].z_size - 1;
		while (mid > 7) {
			mid /= 2;
			if (is_less_than(c->get_value(val), c->get_value(c->w_axis[w].x_axis[x].y_axis[y].z_values[z-mid]))) {
				z -= mid;
			}
		}
		while (z>=0 && is_less_than(c->get_value(val), c->get_value(c->w_axis[w].x_axis[x].y_axis[y].z_values[z]))) {
			z--;
		}
		z++;
	}

	/*
		Perform the insertion
								*/

	// Increase the size of our Z axis
	c->w_axis[w].x_axis[x].y_axis[y].z_size++;
	c->w_axis[w].x_axis[x].y_axis[y].z_values = realloc(
		c->w_axis[w].x_axis[x].y_axis[y].z_values,
		c->w_axis[w].x_axis[x].y_axis[y].z_size * sizeof(void*)
		);


	// Check to see if the position of the new element is at the end of z-axis
	if (z+1 != c->w_axis[w].x_axis[x].y_axis[y].z_size) {
		// Shift the Z-axis over a notch
		memmove(
			&c->w_axis[w].x_axis[x].y_axis[y].z_values[z+1],
			&c->w_axis[w].x_axis[x].y_axis[y].z_values[z],
			(c->w_axis[w].x_axis[x].y_axis[y].z_size - z - 1) * sizeof(void*)
		);
	}

	// Set the value to point to the original item
	c->w_axis[w].x_axis[x].y_axis[y].z_values[z] = val;
	insert(NULL, 0);

	// Split if necessary
	if (c->w_axis[w].x_axis[x].y_axis[y].z_size == c->axis_length) {
		y_split(c, w, x, y);
		if (c->w_axis[w].x_axis[x].y_size == c->axis_length) {
			x_split(c, w, x);
			if (c->w_axis[w].x_size == c->axis_length) {
				w_split(c, w);
				if (c->w_size == c->axis_length) {
					c->axis_length += 2;
				}
			}
		}
	}
	// Increase the volume counter
	c->volume++;
}

//
// Insertion/split functions
//

void w_insert(cube* c, unsigned int w) {

	// Increase the size of the array
	c->w_size++;
	c->w_axis = realloc(c->w_axis, c->w_size * sizeof(cube_w));

	// If it's not the last item in the axis, shift everything else down first
	if(c->w_size != w+1){
		memmove(
			&c->w_axis[w+1],
			&c->w_axis[w],
			(c->w_size - w - 1) * sizeof(cube_w)
			);
	}

	// The new node is empty, let's set it
	c->w_axis[w+1].x_size = 0;
	c->w_axis[w+1].w_floor = 0;
}

void w_split(cube* c, unsigned int w) {

	// Insert a new empty axis
	w_insert(c, w);

	// Adjust the size of the two X-axes
	c->w_axis[w+1].x_size = c->w_axis[w].x_size/2;
	c->w_axis[w].x_size -= c->w_axis[w+1].x_size;

	// Allocate the memory
	c->w_axis[w+1].x_axis = malloc(c->w_axis[w+1].x_size * sizeof(cube_x));

	// Move the Y nodes to the new Y-axis
	memmove(
		c->w_axis[w+1].x_axis,
		&c->w_axis[w].x_axis[c->w_axis[w].x_size],
		c->w_axis[w+1].x_size * sizeof(cube_x)
		);

	// Set the new floor
	c->w_axis[w+1].w_floor = c->w_axis[w+1].x_axis[0].x_floor;

	// Resize the original axis to its new size
	c->w_axis[w].x_axis = realloc(c->w_axis[w].x_axis, c->w_axis[w].x_size * sizeof(cube_x));
}

void x_insert(cube_w* w, unsigned int x) {

	// Increase the size of the array
	w->x_size++;
	w->x_axis = realloc(w->x_axis, w->x_size * sizeof(cube_x));

	// If it's not the last item in the axis, shift everything else down first
	if(w->x_size != x+1){
		memmove(
			&w->x_axis[x+1],
			&w->x_axis[x],
			(w->x_size - x - 1) * sizeof(cube_x)
			);
	}

	// The new node is empty, let's set it
	w->x_axis[x+1].y_size = 0;
	w->x_axis[x+1].x_floor = 0;
}

void x_split(cube* c, unsigned int w, unsigned int x) {

	// Insert a new empty axis
	x_insert(&c->w_axis[w], x);

	// Adjust the size of the two X-axes
	c->w_axis[w].x_axis[x+1].y_size = c->w_axis[w].x_axis[x].y_size/2;
	c->w_axis[w].x_axis[x].y_size -= c->w_axis[w].x_axis[x+1].y_size;

	// Allocate the memory
	c->w_axis[w].x_axis[x+1].y_axis = malloc(c->w_axis[w].x_axis[x+1].y_size * sizeof(cube_y));

	// Move the Y nodes to the new Y-axis
	memmove(
		c->w_axis[w].x_axis[x+1].y_axis,
		&c->w_axis[w].x_axis[x].y_axis[c->w_axis[w].x_axis[x].y_size],
		c->w_axis[w].x_axis[x+1].y_size * sizeof(cube_y)
		);

	// Set the new floor
	c->w_axis[w].x_axis[x+1].x_floor = c->w_axis[w].x_axis[x+1].y_axis[0].y_floor;

	// Resize the original axis to its new size
	c->w_axis[w].x_axis[x].y_axis = realloc(c->w_axis[w].x_axis[x].y_axis, c->w_axis[w].x_axis[x].y_size * sizeof(cube_y));
}

void y_insert(cube_x* x, unsigned int y) {

	// Increase the size of the array
	x->y_size++;
	x->y_axis = realloc(x->y_axis, x->y_size * sizeof(cube_y));

	// If it's not the last item in the axis, shift everything else down first
	if(x->y_size != y+1){
		memmove(&x->y_axis[y+1], &x->y_axis[y], (x->y_size - y - 1) * sizeof(cube_y));
	}

	// The new node is empty, let's set it
	x->y_axis[y+1].z_size = 0;
}

void y_split(cube* c, unsigned int w, unsigned int x, unsigned int y) {
	y_insert(&c->w_axis[w].x_axis[x], y);

	// Adjust the sizes of the two axes
	c->w_axis[w].x_axis[x].y_axis[y+1].z_size = c->w_axis[w].x_axis[x].y_axis[y].z_size/2;
	c->w_axis[w].x_axis[x].y_axis[y].z_size -= c->w_axis[w].x_axis[x].y_axis[y+1].z_size;

	c->w_axis[w].x_axis[x].y_axis[y+1].z_values = malloc(c->w_axis[w].x_axis[x].y_axis[y+1].z_size * sizeof(void*));

	// Copy the memory of half the axis over
	memcpy(
		c->w_axis[w].x_axis[x].y_axis[y+1].z_values,
		&c->w_axis[w].x_axis[x].y_axis[y].z_values[c->w_axis[w].x_axis[x].y_axis[y].z_size],
		c->w_axis[w].x_axis[x].y_axis[y+1].z_size * sizeof(void*)
		);

	// Set the floor
	c->w_axis[w].x_axis[x].y_axis[y+1].y_floor = c->get_value(c->w_axis[w].x_axis[x].y_axis[y+1].z_values[0]);

	// Resize the original axis to its new size
	c->w_axis[w].x_axis[x].y_axis[y].z_values = realloc(
		c->w_axis[w].x_axis[x].y_axis[y].z_values,
		c->w_axis[w].x_axis[x].y_axis[y].z_size * sizeof(void*)
		);
}

// Print a cube, will default to the int data type.
void print_cube(cube* c) {
	printf("Cube volume: %i\nCube max axis length: %i\n", c->volume, c->axis_length);
	for (int w=0; w<c->w_size; w++) {
		printf("W: %i:%i\tF: %i\n", w, c->w_axis[w].x_size, c->w_axis[w].w_floor);
		for (int x=0; x<c->w_axis[w].x_size; x++) {
			printf("\tX: %i:%i\tF: %i\n", x, c->w_axis[w].x_axis[x].y_size, c->w_axis[w].x_axis[x].x_floor);
			for (int y=0; y<c->w_axis[w].x_axis[x].y_size; y++) {
				printf("\t\tY: %i:%i\tF: %i\n\t\t[", y, c->w_axis[w].x_axis[x].y_axis[y].z_size, c->w_axis[w].x_axis[x].y_axis[y].y_floor);
				for (int z=0; z<c->w_axis[w].x_axis[x].y_axis[y].z_size; z++) {
					printf("%i", c->get_value(c->w_axis[w].x_axis[x].y_axis[y].z_values[z]));
					if (z != c->w_axis[w].x_axis[x].y_axis[y].z_size-1) {
						printf(", ");
					}
				}
				printf("]\n");
			}
		}
	}
}

// Frees all the heap memory used by the cube
void clear_cube(cube* c) {
	for (int w=0; w<c->w_size; w++) {
		for (int x=0; x<c->w_axis[w].x_size; x++) {
			for (int y=0; y<c->w_axis[w].x_axis[x].y_size; y++) {
				free(c->w_axis[w].x_axis[x].y_axis[y].z_values);
			}
			free(c->w_axis[w].x_axis[x].y_axis);
		}
		free(c->w_axis[w].x_axis);
	}
	free(c->w_axis);
	cube_init(c, c->get_value, c->set_value);
}

// Same as above, but also frees the cube itself (if needed)
void delete_cube(cube* c) {
	clear_cube(c);
	free(c);
	c = NULL;
}

// Traverse a cube in order and place all of its values in an array
// using the set_value() function defined in cube_init()
void cube_to_array(cube* c, void* p, int len) {
	int i = 0;
	// Loop through all elements
	for (int w=0; w<c->w_size; w++) {
		for (int x=0; x<c->w_axis[w].x_size; x++) {
			for (int y=0; y<c->w_axis[w].x_axis[x].y_size; y++) {
				for (int z=0; z<c->w_axis[w].x_axis[x].y_axis[y].z_size; z++) {
					// Set the value in the destination pointer by using the set_value() function to cast the correct type
					c->set_value(
						c->w_axis[w].x_axis[x].y_axis[y].z_values[z],
						p + (i++ * sizeof(c->get_value(c->w_axis[w].x_axis[x].y_axis[y].z_values[z])))
						);
				}
			}
		}
	}
}

// Reformat a cube into its most balanced structure by making all axes as equal as possible
void rebalance_cube(cube* c) {
	int root, high;
	int w, x , y, z;
	int w2, x2, y2, z2;
	cube c2;

	// Calculate quartic root
	root = 0;
	high = c->volume;
	while (root*root*root*root < high) {
		root++;
	}
	root = (((root+2)>>1)) << 1;
	// Give it a minimum
	root = root > BSC_LENGTH ? root : BSC_LENGTH;
	
	// Create a new temp cube
	cube_init(&c2, c->get_value, c->set_value);
	c2.axis_length = root;

	w2 = x2 = y2 = z2 = 0;

	c2.w_axis = malloc(root * sizeof(cube_w));
	c2.w_axis[w2].x_axis = malloc(root * sizeof(cube_x));
	c2.w_axis[w2].x_axis[x2].y_axis = malloc(root * sizeof(cube_y));
	c2.w_axis[w2].x_axis[x2].y_axis[y2].z_values = malloc(root * sizeof(void*));

	// Set the sizes
	c2.w_size = c2.w_axis[w2].x_size = c2.w_axis[w2].x_axis[x2].y_size = c2.w_axis[w2].x_axis[x2].y_axis[y2].z_size = 0;

	high = 0;
	for (w=0; w<c->w_size && c->volume-high; w++) {
		for (x=0; x<c->w_axis[w].x_size && c->volume-high; x++) {
			for (y=0; y<c->w_axis[w].x_axis[x].y_size && c->volume-high; y++) {
				for (z=0; z<c->w_axis[w].x_axis[x].y_axis[y].z_size && c->volume-high; z++) {
					// Set the value
					c2.w_axis[w2].x_axis[x2].y_axis[y2].z_values[z2] = c->w_axis[w].x_axis[x].y_axis[y].z_values[z];
					// Check to see if we need to reposition each axis, then prepare the next one
					z2++;
					high++;
					c2.w_axis[w2].x_axis[x2].y_axis[y2].z_size++;
					if (z2 == root-1 && c->volume-high) {
						z2 = 0;
						y2++;
						c2.w_axis[w2].x_axis[x2].y_size++;
						c2.w_axis[w2].x_axis[x2].y_axis[y2].z_values = malloc(root * sizeof(void*));
						c2.w_axis[w2].x_axis[x2].y_axis[y2].z_size = 0;
						if (y2 == root-1) {
							y2 = 0;
							x2++;
							c2.w_axis[w2].x_size++;
							c2.w_axis[w2].x_axis[x2].y_axis = malloc(root * sizeof(cube_y));
							c2.w_axis[w2].x_axis[x2].y_size = 0;
							c2.w_axis[w2].x_axis[x2].y_axis[y2].z_values = malloc(root * sizeof(void*));
							c2.w_axis[w2].x_axis[x2].y_axis[y2].z_size = 0;
							if (x2 == root-1) {
								x2 = 0;
								w2++;
								c2.w_size++;
								c2.w_axis[w2].x_axis = malloc(root * sizeof(cube_x));
								c2.w_axis[w2].x_size = 0;
								c2.w_axis[w2].x_axis[x2].y_axis = malloc(root * sizeof(cube_y));
								c2.w_axis[w2].x_axis[x2].y_size = 0;
								c2.w_axis[w2].x_axis[x2].y_axis[y2].z_values = malloc(root * sizeof(void*));
								c2.w_axis[w2].x_axis[x2].y_axis[y2].z_size = 0;
							}
						}
					}
				}
			}
		}
	}
	// Add one to each final axis
	c2.w_axis[w2].x_axis[x2].y_size++;
	c2.w_axis[w2].x_size++;
	c2.w_size++;
	
	// Switch out the W-axes
	clear_cube(c);
	c->w_axis = c2.w_axis;
	c->w_size = c2.w_size;
	c->axis_length = root;
}