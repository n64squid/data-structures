/* ==========================================================================
	 ____  _ _
	|  _ \(_) | ___  ___   ___ 
	| |_) | | |/ _ \/ __| / __|
	|  __/| | |  __/\__ \| (__ 
	|_|   |_|_|\___||___(_)___|

	Piles are a data structure that work like a 2D array, except that instead
	of having fixed lenths in both directions, the size of the 2nd dimension
	can vary from pile to pile.

 ========================================================================== */

#include "basic.h"
#include "get_set_funcs.h"
#include "piles.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// A function to print the current status of the piles on the board
void piles_print(piles* p) {
	int i, j;
	printf("Piles: %i\n", p->len);
	// Loop through piles
	for (i=0; i<p->len; i++) {
		printf("%i\t[",p->piles_len[i]);
		// Loop through elements
		for (j=0; j<p->piles_len[i]; j++) {
			if (j == p->piles_len[i]-1) {
				printf("%i", get_value_int(p->piles[i][j]));
			} else {
				printf("%i, ", get_value_int(p->piles[i][j]));
			}
		}
		printf("]\n");
	}
}

// Initialise all values in the pile to zero
void piles_init(piles* p, char(**compare_funcs)(void*, void*)) {
	p->len = 0;
	p->piles = NULL;
	p->piles_len = NULL;
	p->compare_funcs = compare_funcs;
}

// Creates a new pile at the end
void piles_add_pile(piles* p) {
	// Check to see if there are any existing piles
	p->len++;
	p->piles = realloc(p->piles, p->len * sizeof(void**));
	p->piles[p->len-1] = NULL;
	// Add a new item to length counter and make it zero
	p->piles_len = realloc(p->piles_len, p->len * sizeof(int));
	p->piles_len[p->len-1] = 0;
}

// Creates many new piles
void piles_add_pile_many(piles* p, int new_piles) {
	for (int i=0; i<new_piles; i++) {
		piles_add_pile(p);
	}
}

// Appends a value to a particular pile's index
void piles_add_item_to_pile(piles* p, int pile, void* value) {
	if (pile >= p->len || pile < 0) {
		piles_add_pile(p);
		pile = p->len-1;
	}
	p->piles_len[pile]++;
	p->piles[pile] = realloc(p->piles[pile], p->piles_len[pile] * sizeof(void*));
	p->piles[pile][p->piles_len[pile]-1] = value;
}

void piles_clear(piles* p) {
	for (int i=0; i<p->len; i++) {
		free(p->piles[i]);
		p->piles[i] = NULL;
	}
	free(p->piles_len);
	p->piles_len = NULL;
	free(p->piles);
	p->piles = NULL;
}

// Clear the pile and the pile pointer as well
void piles_delete(piles* p) {
	piles_clear(p);
	free(p);
	p = NULL;
}

// Insert piles into an array in a linear fashion
// Fast, but requires piles to be sorted internally and between each other
void piles_to_array(piles* pile, int* a, char size) {
	int k = 0;			// Array position counter
	int i, j;			// Loop iterators
	char* temp = NULL;	// Temp array to hold the output
	int templen = 0;	// Length of temp array

	// Loop through piles
	for (i=0; i<pile->len; i++) {
		// Reallocate the memory
		templen += pile->piles_len[i];
		temp = realloc(temp, templen * size);
		// Loop through each element in the pile
		for (j=0; j<pile->piles_len[i]; j++) {
			// Insert it into the array
			memcpy(&temp[(k++)*size], pile->piles[i][j], size);
			insert(NULL, 0);
		}
	}
	memcpy(a, temp, templen * size);
	free(temp);
}

// Puts piles into an array using Merge Sort
// Slower, but it piles only need to be sorted internally
void piles_merge_to_array(piles* pile, void* a, char size) {
	int i, it;			// Loop iterator
	int i1, i2;			// Pile temp index
	void** temp;		// Temporary array to hold the merging lists
	void* output;
	piles p;

	// Make a copy of the pile
	memcpy(&p, pile, sizeof(piles));
	// Copy the pile lengths
	p.piles_len = malloc(p.len * sizeof(int));
	memcpy(p.piles_len, pile->piles_len, p.len * sizeof(int));
	// Copy the piles
	p.piles = malloc(p.len * sizeof(void**));
	for (i=0; i<p.len; i++) {
		p.piles[i] = malloc(p.piles_len[i] * sizeof(void*));
		memcpy(p.piles[i], pile->piles[i], p.piles_len[i] * sizeof(void*));
	}

	// As long as there is more than one pile
	while(p.len > 1) {

		// Take the first and last pile
		for(i=0; i<p.len/2; i++) {
			i1 = i2 = 0;
			temp = calloc((p.piles_len[i] + p.piles_len[p.len-1-i]), sizeof(void*));
			// Loop through each element on each list
			it = 0; // Index for the temp array
			while (i1 < p.piles_len[i] && i2 < p.piles_len[p.len-1-i]) {

				if ((*p.compare_funcs[COMPARE_LESS_THAN])(p.piles[i][i1], p.piles[p.len-1-i][i2])) {
					// Insert from a1
					memcpy(&temp[it], &p.piles[i][i1], sizeof(void*));
					insert(NULL, 0);
					i1++;
					// Check for end
					if (i1 >= p.piles_len[i]) {
						// Fill in the rest of the array with a2
						for (; i2<p.piles_len[p.len-1-i]; i2++) {
							memcpy(&temp[++it], &p.piles[p.len-1-i][i2], sizeof(void*));
							insert(NULL, 0);
						}
					}
				} else {
					// Insert from a2
					memcpy(&temp[it], &p.piles[p.len-1-i][i2], sizeof(void*));
					insert(NULL, 0);
					i2++;
					// Check for end
					if (i2 >= p.piles_len[p.len-1-i]) {
						// Fill in the rest of the array with a1
						for (; i1<p.piles_len[i]; i1++) {
							memcpy(&temp[++it], &p.piles[i][i1], sizeof(void*));
							insert(NULL, 0);
						}
					}
				}
				it++;
			}
			// Clean things up
			// Increase the size of a1 to accomodate the merge
			p.piles_len[i] += p.piles_len[p.len-1-i];
			p.piles[i] = realloc(p.piles[i], p.piles_len[i] * sizeof(void*));
			// Set the original array to its new values
			memcpy(p.piles[i], temp, p.piles_len[i] * sizeof(void*));
			// Free memory
			free(temp);
			free(p.piles[p.len-1-i]);
		}
		p.len -= p.len/2;
	}

	// Insert into temporary output in the correct order
	output = malloc(p.piles_len[0] * size);
	for (i=0; i<p.piles_len[0]; i++) {
		memcpy(output+(i*size), p.piles[0][i], size);
	}
	// Now copy from output array into the final array
	memcpy(a, output, p.piles_len[0] * size);

	// Free memory
	piles_clear(&p);
	free(output);
}