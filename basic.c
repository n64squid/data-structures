/* ====================================================================================
	 ____            _            
	| __ )  __ _ ___(_) ___   ___ 
	|  _ \ / _` / __| |/ __| / __|
	| |_) | (_| \__ | | (__ | (__ 
	|____/ \__,_|___|_|\___(_\___|

	The placeholder for other files that might use this functionality.

	There are also variations of each function with pointer versions instead of ints.

 ==================================================================================== */

#include <stdio.h>

void swap(int* a, int* b) {
	if (a && b)
		*a ^= *b ^= *a ^=*b;
}

void insert(int* a, int b) {
	if (a)
		*a = b;
}

// Compare two items, 'less than' version
char is_less_than(int a, int b) {
	return (a < b);
}
char is_less_than_p(int* a, int* b) {
	return (*a < *b);
}

// Compare two items, 'greater than' version
char is_greater_than(int a, int b) {
	return (a > b);
}
char is_greater_than(int* a, int* b) {
	return (*a > *b);
}

// Compare two items, 'equal to' version
char is_equal_to(int a, int b) {
	return (a == b);
}
char is_equal_to_p(int* a, int* b) {
	return (*a == *b);
}

char is_greater_than_or_equal_to(int a, int b) {
	return (a >= b);
}
char is_greater_than_or_equal_to_p(int* a, int* b) {
	return (*a >= *b);
}

char is_less_than_or_equal_to(int a, int b) {
	return (a <= b);
}
char is_less_than_or_equal_to_p(int* a, int* b) {
	return (*a <= *b);
}

int read_val(int a) {
	return a;
}
int read_val_p(int* a) {
	return *a;
}