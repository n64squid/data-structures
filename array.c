/* ==========================================================================
	    _
	   / \   _ __ _ __ __ _ _   _   ___
	  / _ \ | '__| '__/ _` | | | | / __|
	 / ___ \| |  | | | (_| | |_| || (__
	/_/   \_|_|  |_|  \__,_|\__, (_\___|
	                        |___/

	Functions for managing standard arrays

 ========================================================================== */

// Functions for managing standard arrays

#include "array.h"
#include "basic.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

// Print an array
void print_list(int *p, int len) {
	printf("[");
	for (int i=0; i<len; i++) {
		if (i == len-1) {
			printf("%i", p[i]);
		} else {
			printf("%i, ", p[i]);
		}
	}
	printf("]\n");
}

// Get the average (mean) of an array
unsigned long long array_average(unsigned long long *p, int len) {
	unsigned long long sum = 0;
	for (int i=0; i<len; i++) {
		sum += p[i];
	}
	return sum/len;
}

void set_seed(int seed) {
	// Get time in ms
	struct timeval current_time;
	gettimeofday(&current_time, NULL);

	// Generate a random seed
	srand ((unsigned)(current_time.tv_sec+current_time.tv_usec+seed));
}

// Shuffle an array
void shuffle(int *p, int len) {
	int i;		// Loop iterator
	int s;		// Element with which to swap

	// Loop through elements and swap them if needed
	for (i=0; i<len-1; i++) {
		s = rand() % (len-i);
		if (s) {
			swap(&p[i], &p[i+s]);
		}
	}
}

// Shuffle an array using a perfect shuffle. It gets mixed up a bit without having to rely on randomness.
void perfect_shuffle(int* p, int len, unsigned char type) {
	int* left;
	int* right;
	int i, j;

	// Allocate memory
	left = malloc((len/2) * sizeof(int));
	right = malloc((len - len/2) * sizeof(int));

	// Copy values
	memcpy(left, p, (len/2) * sizeof(int));
	memcpy(right, &p[len/2], (len - len/2) * sizeof(int));

	// Set the counter for the original array
	j = 0;
	// Loop through each element in the auxiliary arrays
	// Perform an in-shuffle
	if (type == PERFECT_SHUFFLE_IN) {
		for (i=0; i<len/2; i++) {
			insert(&p[j++], right[i]);
			insert(&p[j++], left[i]);
		}
	}
	// Perform an out-shuffle
	if (type == PERFECT_SHUFFLE_OUT) {
		for (i=0; i<len/2; i++) {
			insert(&p[j++], left[i]);
			insert(&p[j++], right[i]);
		}
	}

	// Clear the memory
	free(left);
	free(right);
}

// Returns the index of the smallest value
int array_min(int *p, int len) {
	int min = 0;
	for (int i=1; i<len; i++) {
		min = is_less_than_p(&p[i], &p[min]) ? i : min;
	}
	return min;
}

// Returns the index of the largest value
int array_max(int *p, int len) {
	int max = 0;
	for (int i=1; i<len; i++) {
		max = is_greater_than_p(&p[i], &p[max]) ? i : max;
	}
	return max;
}

// Check to see if a list is sorted
char check_list(int *p, int len, char print) {
	for (int i=1; i<len; i++) {
		if (p[i] < p[i-1]){
			if (print) {
				printf("Failed check at position %i, (%i<%i)\n", i-1, p[i], p[i-1]);
			}
			return 0;
		}
	}
	if (print) {
		printf("List is in order! :)\n");
	}
	return 1;
}

// Generate arrays

int* generate_ascending_array(int* p, int len) {
	p = realloc(p, len * sizeof(int));
	for (int i=0; i<len; i++) {
		insert(&p[i], i);
	}
	return p;
}

int* generate_almost_sorted_array(int* p, int len) {
	p = generate_ascending_array(p, len);
	for (int i=0; i<sqrt(len); i++) {
		swap(&p[rand() % (len-1)], &p[rand() % (len-1)]);
	}
	return p;
}

int* generate_reverse_array(int* p, int len) {
	p = realloc(p, len * sizeof(int));
	for (int i=0; i<len; i++) {
		insert(&p[i], len-i-1);
	}
	return p;
}

int* generate_repeating_array(int* p, int len) {
	// Set divisor to a positive integer
	int divisor = sqrt(len);
	p = realloc(p, len * sizeof(int));
	for (int i=0; i<len; i++) {
		insert(&p[i],(i/divisor)*divisor);
	}
	return p;
}

int* generate_random_array(int* p, int len) {
	// Generate the array
	p = generate_ascending_array(p, len);
	shuffle(p, len);
	return p;
}

int* generate_random_random_array(int* p, int len) {
	// Generate the array
	p = realloc(p, len * sizeof(int));
	for (int i=0; i<len; i++) {
		insert(&p[i], rand() % (len-1));
	}
	return p;
}

int* generate_random_repeating_array(int* p, int len) {
	// Generate the array
	p = generate_repeating_array(p, len);
	for (int i=0; i<len; i++) {
		swap(&p[i], &p[(rand() % (len-i)) + i]);
	}
	return p;
}