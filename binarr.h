// Functions for managing binary arrays

#ifndef _BINARR_H_
#define _BINARR_H_
#define BINARR_0 0x00
#define BINARR_1 0x01

// Define the binarr structure
typedef struct {
	unsigned char* p;
	int len;
} binarr;

// Function prototypes
void binarr_init (binarr* b, int len);

void binarr_set (binarr* b, int pos, char val);

void binarr_flip (binarr* b, int pos);

int binarr_get (binarr* b, int pos);

void binarr_print (binarr* b);
void binarr_2d_print (binarr* b, int max_len);

void binarr_shift (binarr* b, int shifts);

void binarr_free (binarr* b);

#endif // BINARR_H_