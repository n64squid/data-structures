/* ==========================================================================
	 _____
	|_   _| __ ___  ___   ___
	  | || '__/ _ \/ _ \ / __|
	  | || | |  __/  __/| (__
	  |_||_|  \___|\___(_)___|

	This is a reusable data structure for binary trees. Binary trees consist
	of nodes tht contain a value (in this case, an integer) and two pointers
	to its child nodes.

	The child node on the left must be lower or equal to its parent, and the
	right child must be greater than or equal to its parent.

	This file also contains splays, which are very similar to binary trees,
	but they insert new items at the top rather than the bottom. This makes
	recent items much quicker to access.

 ========================================================================== */

#include "tree.h"
#include "basic.h"
#include "get_set_funcs.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Initialises a tree data structure
void tree_init(tree* t, char(**compare_funcs)(void*, void*)) {
	t->root = NULL;
	t->node_count = 0;
	t->compare_funcs = compare_funcs;
}

// Function to create a new node
tree_node* tree_new_node(void* new_key) {
	tree_node* n = (tree_node*)malloc(sizeof(tree_node));
	n->val = new_key;
	n->left = n->right = NULL;
	return (n);
}

static void tree_insert_recursive(tree_node* n, void* p, char(**compare_funcs)(void*, void*)) {
	// Check to see if we're at the end of the tree
	if ((*compare_funcs[COMPARE_LESS_THAN_EQUAL_TO])(p, n->val)) {
		// Move leftwards
		if (n->left == NULL) {
			// End of line, create new node
			n->left = tree_new_node(p);
		} else {
			// Carry on with search
			tree_insert_recursive(n->left, p, compare_funcs);
		}
	} else {
		// Move rightwards
		if (n->right == NULL) {
			// End of line, create new node
			n->right = tree_new_node(p);
		} else {
			// Carry on with search
			tree_insert_recursive(n->right, p, compare_funcs);
		}
	}
}

// Insert an item into the tree structure
void tree_insert(tree* t, void* p) {
	// Check to see if the tree itself has been initialised
	if (t->root) {
		// Root exists, follow normal process
		tree_insert_recursive(t->root, p, t->compare_funcs);
	} else {
		// Tree is empty, let's start it
		t->root = tree_new_node(p);
	}
	t->node_count++;
}

// Traverse the tree in_order and place values in an array
static void tree_in_order_recursive(tree_node* n, void* p, int level, char(**compare_funcs)(void*, void*), unsigned char size) {
	static int index = 0;	// Index for where to put the element in the array
	// Has a left child?
	if (n->left != NULL) {
		// Load left child node
		tree_in_order_recursive(n->left, p, level+1, compare_funcs, size);
	}

	// Insert the value of the pointer into the array
	memcpy(p+(size*index++), n->val, size);
	insert(NULL, 0);

	// Has unprinted right child?
	if (n->right != NULL && (*compare_funcs[COMPARE_GREATER_THAN_EQUAL_TO])(n->right->val, p+(size*(index-1)))) {
		tree_in_order_recursive(n->right, p, level+1, compare_funcs, size);
	} else {
		// Return to parent
		if (level == 0) {
			index = 0;
		}
		return;
	}
	
	// Already Printed?
	if ((*compare_funcs[COMPARE_LESS_THAN_EQUAL_TO])(n->right->val, p+(size*(index-1)))) {
		// Load parent
		if (level == 0) {
			// Reset the index to zero when we are done
			index = 0;
		}
		return;
	} else {
		// Insert the value of the pointer into the array
		memcpy(p+(size*index++), n->val, size);
		insert(NULL, 0);
	}
}

// Wrapper function to simplify things
void tree_in_order(tree* t, void* p, unsigned char size) {
	char* temp = malloc(t->node_count * size);	// Temp array to avoid overlaps
	tree_in_order_recursive(t->root, temp, 0, t->compare_funcs, size);
	memcpy(p, temp, t->node_count * size);
	free(temp);
}

static void tree_delete_recursive(tree_node* n) {
	// Do nothing if node does not exist
	if (n == NULL) {return;}

	// Continue recursively along each branch
	tree_delete_recursive(n->left);
	tree_delete_recursive(n->right);

	// Delete node
	free(n);
	return;
}

// Free the memory of all nodes in a tree
void tree_delete(tree* t) {
	tree_delete_recursive(t->root);
}

/* =======================================================================
	 ____
	/ ___| _ __ | | __ _ _   _
	\___ \| '_ \| |/ _` | | | |
	 ___) | |_) | | (_| | |_| |
	|____/| .__/|_|\__,_|\__, |
	      |_|            |___/

	These are functions relating specifically to splays.

========================================================================== */

// Rotating function
tree_node* tree_rotate_right(tree_node* x) {
	tree_node *y = x->left;
	x->left = y->right;
	y->right = x;
	return y;
}

tree_node* tree_rotate_left(tree_node* x) {
	tree_node* y = x->right;
	x->right = y->left;
	y->left = x;
	return y;
}

// Perform a splay on the tree. This will modify the tree and return the new root.
tree_node* tree_splay(tree* t, tree_node* root, void* new_key) {

	// Check to see if root is either NULL or already the value we want
	if (root == NULL || new_key == root->val) {
		return root;
	}

	if ((*t->compare_funcs[COMPARE_LESS_THAN])(new_key, root->val)) {
		// Check out left branch
		// Check if left branch is empty
		if (root->left == NULL) {
			// Nothing to see here
			return root;
		}

		// Do some zigging and zagging
		if ((*t->compare_funcs[COMPARE_LESS_THAN])(new_key, root->left->val)) {
			// Zig-Zig (left/left)
			root->left->left = tree_splay(t, root->left->left, new_key);
			root = tree_rotate_right(root);
		} else if (new_key > root->left->val) {
			// Zig-zag (left/right)
			root->left->right = tree_splay(t, root->left->right, new_key);
			if (root->left->right != NULL) {
				root->left = tree_rotate_left(root->left);
			}
		}

		// Do a second rotation
		if (root->left == NULL) {
			return root;
		} else {
			return tree_rotate_right(root);
		}

	} else {
		// Check out right branch
		// Check if right branch is empty
		if (root->right == NULL) {
			// Nothing to see here
			return root;
		}

		// Do some zigging and zagging
		if ((*t->compare_funcs[COMPARE_LESS_THAN])(new_key, root->right->val)) {
			// Zag-Zig (right/left)
			root->right->left = tree_splay(t, root->right->left, new_key);
			if (root->right->left != NULL) {
				root->right = tree_rotate_right(root->right);
			}
		} else if ((*t->compare_funcs[COMPARE_GREATER_THAN])(new_key, root->right->val)) {
			// Zag-zag (right/right)
			root->right->right = tree_splay(t, root->right->right, new_key);
			root = tree_rotate_left(root);
		}

		// Do a second rotation
		if (root->right == NULL) {
			return root;
		} else {
			return tree_rotate_left(root);
		}
	}
}

// Insert a new node into the splay
void tree_splay_insert(tree* t, void* new_key) {
	t->node_count++;
	// Check for null
	if (t->root == NULL) {
		t->root = tree_new_node(new_key);
		return;
	}
	// Bring the closest leaf node to root
	t->root = tree_splay(t, t->root, new_key);

	// Let's create a new node
	tree_node *n = tree_new_node(new_key);

	// Compare the new_key to the root and move it to either side
	if ((*t->compare_funcs[COMPARE_LESS_THAN])(new_key, t->root->val)) {
		n->right = t->root;
		n->left = t->root->left;
		t->root->left = NULL;
	} else {
		n->left = t->root;
		n->right = t->root->right;
		t->root->right = NULL;
	}

	// Place the new node back at the root
	t->root = n;
}