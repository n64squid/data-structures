# Data structures

This is a repository to hold all the various reusable data structures within my other C projects.

## Binary arrays (Bit fields / Bit vectors)

These are arrays of chars whose purpose is to access the individual bits within them. Think of it kind of like a long strip of 1s and 0s.

It is useful for denoting flags for an array of items or just making an array of booleans a bit smaller. Binary arrays can also be useful when working with hardware registers or other memory-mapped I/O, where each bit may have a specific meaning or function. They can also be used to save space in memory-limited applications, although care must be taken to ensure that the packed data is accessed and manipulated correctly.

## Trees and Splays

A binary tree is a tree data structure in which each node has at most two children, referred to as the left child and the right child. The topmost node of the tree is called the root. Binary trees are commonly used in computer science for searching and sorting algorithms. A binary search tree is a binary tree where the left child is less than the parent and the right child is greater than the parent, making searching and insertion relatively efficient.

A splay tree is a self-adjusting binary search tree where nodes that are frequently accessed are moved closer to the root of the tree, improving the average access time. In a splay tree, each time a node is accessed, it is moved to the root of the tree using a set of rotations. This operation is known as splaying. The splaying process ensures that frequently accessed nodes are at the root of the tree and therefore have faster access times. Splay trees are commonly used in memory allocation and file systems due to their ability to adapt to changes in access patterns over time.

## Piles

Piles are a data structure that is a variation of the 2D array. The difference is that the length of each pile can vary from the next, which does require a bit of memory management to keep track of each length. Most of what is done with the piles structure can be done with a simpler 2D array but because of the way the input can turn out, piles are more memory efficient.

A 2D array will always use *O(n^2)* memory and require some kind of null terminator (or auxiliary array) to keep track of lengths. Meanwhile, piles require *O(n)* to hold the data and *O(log(n))* on average to hold pile lengths (*O(n)* in a worst-case). This makes it a much more memory efficient way to hold variable-length 2D array data.

For 2D arrays where the full width is always fixed (eg, a frame buffer or a tile map), then a 2D array will work just fine.

## Trie

A trie, also known as a prefix tree, is a tree-based data structure used to store a set of strings. Each node in the trie represents a common prefix of one or more strings. The root of the trie represents an empty string, and each path from the root to a leaf node represents a complete string in the set. This version stores ints which can be offset to represent strings.

Each node in the trie has a set of child nodes, each corresponding to a possible next character in the strings. The children are usually stored in an array or map, indexed by the character they represent. By following the path from the root to a leaf node, you can retrieve a string that is stored in the trie.

Tries are commonly used in applications such as search engines and spell checkers, where fast prefix and wildcard matching is required. They also provide efficient space utilization, as common prefixes are shared among multiple strings.