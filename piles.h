// Functions for managing a 2d array of variable length

#ifndef _PILES_H_
#define _PILES_H_

typedef struct {
	void*** piles;							// Array of piles
	int* piles_len;							// Array holding the length of each pile
	int len;								// Count of piles
	char(**compare_funcs)(void*, void*);		// Functions to be used to compare two elements being pointed to
} piles;

// New
void piles_init(piles* p, char(**compare_funcs)(void*, void*));
void piles_add_pile(piles* p);
void piles_add_pile_many(piles* p, int new_piles);
void piles_print(piles* p);
void piles_add_item_to_pile(piles* p, int pile, void* value);
void piles_to_array(piles* pile, int* a, char size);
void piles_merge_to_array(piles* pile, void* a, char size);
void piles_clear(piles* p);
void piles_delete(piles* p);

#endif // _PILES_H_