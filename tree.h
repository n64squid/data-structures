// Functions for managing binary arrays

#ifndef _TREE_H_
#define _TREE_H_

// Define the tree structure
typedef struct tree_node tree_node;

typedef struct {
	tree_node* root;						// Pointer to the root node
	unsigned int node_count;				// Total nodes in this tree
	char(**compare_funcs)(void*, void*);	// Function used to convert the struct into a signed int
} tree;

struct tree_node {
	void* val;
	tree_node* left,* right; 
};

// Function prototypes
void tree_init(tree* t, char(**compare_funcs)(void*, void*));
void tree_insert(tree* t, void* p);
void tree_in_order(tree* t, void* p, unsigned char size);
void tree_delete(tree* t);

void tree_splay_insert(tree* t, void* new_key);

#endif // _TREE_H_