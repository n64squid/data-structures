#ifndef _TRIE_H_
#define _TRIE_H_

// Define the trie structure
typedef struct trie trie;
struct trie {
	int value;		// Value held in the trie (typically a counter)
	char size;		// Size (base) of this trie structure. Must be equal in all children
	char offset;	// Offset used when trie values don't start at zero
	trie** trie;	// Array of trie pointers to point to its children
};

// Trie-related functions

void trie_init (trie* t, char size, char offset);
void print_trie (trie* t);
int trie_insert (trie* t, int value, int digits);
void trie_to_array (trie* t, int* p, int max_depth);
void trie_clear (trie* t);
void trie_delete (trie* t);

#endif // _TRIE_H_