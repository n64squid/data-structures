#ifndef _BASIC_DATA_STRUCT_FUNCS_H_
#define _BASIC_DATA_STRUCT_FUNCS_H_

// Function prototypes
void swap(int* a, int* b);
void insert(int* a, int b);
char is_less_than(int a, int b);
char is_less_than_p(int* a, int* b);
char is_greater_than(int a, int b);
char is_greater_than_p(int* a, int* b);
char is_equal_to(int a, int b);
char is_equal_to_p(int* a, int* b);
char is_greater_than_or_equal_to(int a, int b);
char is_greater_than_or_equal_to_p(int* a, int* b);
char is_less_than_or_equal_to(int a, int b);
char is_less_than_or_equal_to_p(int* a, int* b);
int read_val(int a);
int read_val_p(int* a);

#endif // _BASIC_DATA_STRUCT_FUNCS_H_