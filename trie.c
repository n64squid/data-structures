/* ==========================================================================
	 _____     _
	|_   _| __(_) ___   ___
	  | || '__| |/ _ \ / __|
	  | || |  | |  __/| (__
	  |_||_|  |_|\___(_)___|

	A trie is a data structure that works like a binary tree but instead of
	having two child nodes, it has any number n nodes. This means that it
	can't use comparators efficiently, but instead it allows for a fast
	assignment of sub-nodes via calculation of powers and logarithms.

 ========================================================================== */

// Functions for managing tries

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "trie.h"
#include "basic.h"

// Initialise a node on the trie structure
void trie_init(trie* t, char size, char offset) {
	// Set the values of the trie's attributes
	t->value = 0;
	t->size = size;
	t->offset = offset;
	// Generate an array of trie pointers and set them all to zero
	t->trie = calloc(size, sizeof(trie*));
}
// Print out all trie elements in order
void print_trie_recursive(trie* t, int level) {
	int i, j;
	for (i=0; i<t->size; i++) {
		if (t->trie[i]) {
			// Do it recursively
			for (j=0; j<level; j++) {
				printf("\t");
			}
			printf("%i:%i\n", i, t->value);
			print_trie_recursive(t->trie[i], level+1);
		} else {
			// End here
		}
	}
}
void print_trie(trie* t) {
	printf("Trie size: %i, offset: %i\n", t->size, t->offset);
	print_trie_recursive(t, 0);
}

int trie_insert_recursive(trie* t, int value, int digits, char level) {
	if (digits+1 == level) {
		return(0);
	}
	int power = (int)pow(t->size, digits-level);
	int digit = ((read_val(value) % (power * t->size))/power);
	trie* next = t->trie[digit];
	
	//printf("Value: %i, Power: %i, Digit: %i, Next:%p\n", value, power, digit, next);
	if (next) {
		// Sub-trie exists already
		if (digits == level) {
			//printf("D:%i, L:%i, V: %i\n", digits, level, value);
			t->trie[digit]->value++;
		}
		return trie_insert_recursive(t->trie[digit], value, digits, level+1);
	} else {
		// Create a new sub-trie by first allocating the struct
		t->trie[digit] = malloc(sizeof(trie));
		// Then initialise it by giving it some default values
		trie_init(t->trie[digit], t->size, t->offset);
		// Increase trie counter
		t->trie[digit]->value++;
		return trie_insert_recursive(t->trie[digit], value, digits, level+1);
	}
	return 0;
}

// Helper function
int trie_insert(trie* t, int value, int digits) {
	return trie_insert_recursive(t, value, digits, 0);
}

int trie_to_array_recursive(trie* t, int* p, int index, int max_depth, int parent_value, char level) {
	int i, j;
	for (i=0; i<t->size; i++) {
		if (t->trie[i]) {
			// Check if we're on the final level
			if (level == max_depth) {
				// Add to array for as many times as we have $value
				for (j=0; j<t->trie[i]->value; j++){
					insert(&p[index++], parent_value + i*(int)pow(t->size, max_depth-level));
				}
			} else {
				// Perform recursively
				index = trie_to_array_recursive(
					t->trie[i],	// Next sub-trie
					p,			// Pass the same array pointer
					index,		// Pass the same index
					max_depth,	// Pass the same max depth
					// Increase parent value by current sub-trie x magnitude
					parent_value + (int)pow(t->size, max_depth-level) * i,
					level+1		// Increase level by one
				);
			}
		}
	}
	// Return index to parent so that it can be sent to its sibling recursions
	return (index);
}
void trie_to_array(trie* t, int* p, int max_depth) {
	trie_to_array_recursive(t, p, 0, max_depth, 0, 0);
}

// Use this to free the contents of the trie
void trie_clear(trie* t) {
	int i;
	for (i=0; i<t->size; i++) {
		if (t->trie[i]) {
			trie_clear(t->trie[i]);
			free(t->trie[i]);
		}
	}
	free(t->trie);
}
// Use this to also free the trie root
void trie_delete(trie* t) {
	trie_clear(t);
	free(t);
}