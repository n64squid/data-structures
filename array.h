// Functions for managing standard arrays a bit more easily

#ifndef _STD_ARRAY_H_
#define _STD_ARRAY_H_

// Constant definitions
#define PERFECT_SHUFFLE_IN 0
#define PERFECT_SHUFFLE_OUT 1

// Prints the list of items
void print_list(int *p, int len);

// Use a new seed based off of the time of day
void set_seed(int seed);

// Shuffles the list randomly
void shuffle(int *p, int len);

// Shuffles the list using a faro shuffle
void perfect_shuffle(int* p, int len, unsigned char type);

// Check to see if the array is sorted
char check_list(int *p, int len, char print);

// Get the min/max index of an array
int array_min(int *p, int len);
int array_max(int *p, int len);

// Return an the average of an array's values, rounded down.
unsigned long long array_average(unsigned long long *p, int len);

// Generate different types of arrays
int* generate_ascending_array(int* p, int len);
int* generate_almost_sorted_array(int* p, int len);
int* generate_reverse_array(int* p, int len);
int* generate_repeating_array(int* p, int len);
int* generate_random_array(int* p, int len);
int* generate_random_random_array(int* p, int len);
int* generate_random_repeating_array(int* p, int len);

#endif // _STD_ARRAY_H_