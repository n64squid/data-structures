#ifndef _GETSETFUNCS_H_
#define _GETSETFUNCS_H_

typedef enum {
	COMPARE_LESS_THAN,
	COMPARE_LESS_THAN_EQUAL_TO,
	COMPARE_EQUAL_TO,
	COMPARE_GREATER_THAN_EQUAL_TO,
	COMPARE_GREATER_THAN
} comparison_t;

// Comparison functions
#define COMPARISON_COUNT 5

// Ints
char compare_int_less_than(void* int1, void* int2);
char compare_int_greater_than(void* int1, void* int2);
char compare_int_equal_to(void* int1, void* int2);
char compare_int_greater_than_or_equal_to(void* int1, void* int2);
char compare_int_less_than_or_equal_to(void* int1, void* int2);
extern char(*compare_int_funcs[])(void*, void*);

// Value conversion retrieval functions
int get_value_int(void* int_pointer);
void set_value_int(void* source, void* dest);

#endif // _GETSETFUNCS_H_