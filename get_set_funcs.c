/* ==========================================================================
	  ____      _   ____       _   ____
	 / ___| ___| |_/ ___|  ___| |_|  __|_   _ _ __   ___ ___   ___
	| |  _ / _ | __\___ \ / _ | __| |_ | | | | '_ \ / __/ __| / __|
	| |_| |  __| |_ ___) |  __| |_|  _|| |_| | | | | (__\__ \| (__
	 \____|\___|\__|____/ \___|\__|_|   \__,_|_| |_|\___|___(_\___|
                                                                

	This is a library of functions used to deal with void pointers.

	The idea is to allow some generalised functions to do the work
	of doing all the void pointer manipulation so that data structures
	can be written to be type-agnostic.

	Each data type should have five comparison functions to compare
	objects of the same type, and some manipulation functions so that
	the objects can be retrieved as needed.

 ========================================================================== */

#include "basic.h"
#include "get_set_funcs.h"

//
// Integers
//

// Enum
char(*compare_int_funcs[])(void*, void*) = {
	compare_int_less_than,
	compare_int_less_than_or_equal_to,
	compare_int_equal_to,
	compare_int_greater_than_or_equal_to,
	compare_int_greater_than
};

// Comparison functions
char compare_int_less_than(void* int1, void* int2) {
	return is_less_than(*(int*)int1, *(int*)int2);
}
char compare_int_less_than_or_equal_to(void* int1, void* int2) {
	return is_less_than_or_equal_to(*(int*)int1, *(int*)int2);
}
char compare_int_equal_to(void* int1, void* int2) {
	return is_equal_to(*(int*)int1, *(int*)int2);
}
char compare_int_greater_than_or_equal_to(void* int1, void* int2) {
	return is_greater_than_or_equal_to(*(int*)int1, *(int*)int2);
}
char compare_int_greater_than(void* int1, void* int2) {
	return is_greater_than(*(int*)int1, *(int*)int2);
}

// Manipulation functions
int get_value_int(void* int_pointer) {
	return *(int*) int_pointer;
}
void set_value_int(void* source, void* dest) {
	*(int*)dest = get_value_int(source);
}
void set_value_void(void* source, void* dest) {
	dest = source;
}