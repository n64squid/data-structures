#ifndef _CUBE_H_
#define _CUBE_H_

// Define some constants
#define BSC_LENGTH 4			// Chunk size for memory allocation
#define BSC_FUNCS_INT 1			// For setting the functions for working with ints

// Struct prototypes/typedefs
typedef struct cube cube;
typedef struct cube_w cube_w;
typedef struct cube_x cube_x;
typedef struct cube_y cube_y;
typedef struct cube_z cube_z;

// Cube structure
struct cube {
	cube_w* w_axis;						// Array of W-axis
	unsigned int w_size;					// Size of the W-axis
	unsigned int axis_length;			// Current max axis length
	unsigned int volume;				
	int(*get_value)(void*);				// Function used to convert the struct into a signed int
	void(*set_value)(void*, void*);		// Function used to convert the struct into a signed int
};

// W-axis node structure
struct cube_w {
	cube_x* x_axis;				// Array of X-axis
	unsigned int x_size;			// Size of the X-axis
	int w_floor;					// Lowest element in this W-axis
};

// X-axis node structure
struct cube_x {
	cube_y* y_axis;				// Array of Y-axis
	unsigned int y_size;			// Size of the Y-axis
	int x_floor;					// Lowest element in this X-axis
};

// Y-axis node structure
struct cube_y {
	void** z_values;				// Array holding the void pointers to the elements in the structure
	unsigned int z_size;			// Size of the X-axis
	int y_floor;					// Lowest element in this Y-axis
};

// Function prototypes
void cube_init(cube* c, int(*get_value)(void*), void(*set_value)(void*, void*));
void cube_insert(cube* c, void* val);
void print_cube(cube* c);
void clear_cube(cube* c);
void delete_cube(cube* c);
void cube_to_array(cube* c, void* p, int len);
void rebalance_cube(cube* c);

#endif // _CUBE_H_